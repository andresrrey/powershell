# Hyper-V Script to change VLAN IDs
# Version 1.0
# Created by Jorge Buitrago
# email: jorge.buitrago@virtesa.com

$vlanid_old = 15
$vlanid_new = 23

$vms = Get-VM
foreach ($vm in $vms){
	$nics=Get-VMNetworkAdapter -VM $vm | ?{$_.VlanSetting.AccessVlanId -eq $vlanid_old}
	foreach ($nic in $nics){
		Set-VMNetworkAdapterVlan -VMNetworkAdapter $nic -Access -VlanId $vlanid_new
	}
}   