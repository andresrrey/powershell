# Powershell script to invoke a script in multiple Hyper-V hosts
# Version 1.0
# Created by Jorge Buitrago
# email: jorge.buitrago@virtesa.com

#This command must be executed in the Powershell Server to establish a PSSession with the Hyper-V hosts:
#  winrm s winrm/config/client '@{TrustedHosts = “<Hyper-V Host>"}'

$HyperV_Hosts = @("172.16.112.138")
$cred = Get-Credential
foreach ($item in $HyperV_Hosts){
	$session=New-PSSession -ComputerName $item -Authentication Negotiate -Credential $cred
	Invoke-Command -Session $session -FilePath C:\Temp\Set-VLAN-ID.ps1
	Remove-PSSession $session
}