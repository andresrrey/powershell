# vSphere ESXi Remediation Script
# Version 1.0
# Configures best practices onto the ESXi hosts
# Created by Jorge Buitrago
# email: jorge.buitrago@virtesa.com

#Initialize Variables
$vCServer = "vcenter.vlab.local" # vCenter Server IP or FQDN
$timeout_TSM = 1200 # Tech Support Mode (TSM) timeout
$timeout_TSM_Interactive = 900 # Idle time before an active Shell session is terminated
$ntp_server = "horalegal.inm.gov.co" # NTP Server
$syslog_server = "syslog.vlab.local" # syslog Server
$syslog_rotate = 15 # Default syslog rotation
$syslog_size = 10240 # Default syslog directory maximum size
$syslog_timeout = 180 # Default timeout to communicate with syslog server
$syslog_local = "/scratch/log" # local path for ESXi logs
$syslog_unique = $true # Unique directory per host?
$dump_collector = "dump.vlab.local" # Dump Collector
$dump_interface = "vmk0" # VMkernel interface used to send Dumps
$dump_port = 6500 # Dump Collector network port
$Allowed_range = "192.168.0.0 - 192.168.0.100" # Range of IPs to allow in the ESXi firewall

Connect-VIServer $vCServer #Connect to vCenter Server

#Get list of active hosts
$esxs = Get-VMHost | ?{$_.ConnectionState -eq "Connected" -or $_.ConnectionState -eq "Maintenance"} | sort

#Loop through the Active ESXi hosts
foreach ($esx in $esxs){

    # ----- TSM Timeout settings -----
    Write-Host "Processing host " $esx.Name
    Write-Host "Current TSM timeout: " (Get-VMHostAdvancedConfiguration -VMHost $esx -Name "Uservars.ESXiShellTimeOut").Values # Get current TSM timeout value
    Write-Host "Current TSM Interactive timeout: " (Get-VMHostAdvancedConfiguration -VMHost $esx -Name "Uservars.ESXiShellInteractiveTimeOut").Values # Get current Interactive timeout value
    Set-VMHostAdvancedConfiguration -VMHost $esx -Name UserVars.ESXiShellTimeOut -Value $timeout_TSM # Set new TSM timeout value
    Set-VMHostAdvancedConfiguration -VMHost $esx -Name UserVars.ESXiShellInteractiveTimeOut -Value $timeout_TSM_Interactive # Set new Interactive timeout
    Write-Host "New TSM Timeout: " (Get-VMHostAdvancedConfiguration -VMHost $esx -Name "Uservars.ESXiShellTimeOut").Values # Get New TSM value
    Write-Host "New TSM Interactive Timeout: " (Get-VMHostAdvancedConfiguration -VMHost $esx -Name "Uservars.ESXiShellInteractiveTimeOut").Values # Get new Interactive value

    # ----- SSH Settings -----
    $ssh = Get-VmHostService -VMhost $esx | Where {$_.Key -eq 'tsm-ssh'} # Get SSH service
    Set-VMHostService -HostService $ssh -policy "off" # Disable SSH
    Stop-VMHostService -HostService $ssh -Confirm:$false # Stop SSH service

    # ----- NTP Server settings -----
    $ntp_list=Get-VmHostNtpServer -VMHost $esx # Get list of configured NTP servers for each host
    #Check if ntp server is already in the list
    if ($ntp_list -contains $ntp_server){
        Write-Host $ntp_server " already in NTP server list"
    }
    else {
        Add-VmHostNtpServer -NtpServer $ntp_server  -VmHost $esx # Add NTP Server to the host's list
        Write-Host "Added NTP Server " $ntp_server "to host $esx"
    }
    #Configure NTP service startup
    $ntp = Get-VmHostService -VMhost $esx | Where {$_.Key -eq 'ntpd'} # Get NTP service
    Set-VMHostService -HostService $ntp -policy "automatic" # Set ntpd to start automatically
    Restart-Vmhostservice $ntp -Confirm:$false # Restart NTP service
    Write-Host "Modified NTP service startup for host $esx"

    # ----- Syslog Settings -----
    $esxcli = Get-ESXCli -VMHost $esx # Get the ESXCli for the ESXi host
    $esxcli.system.syslog.config.set($syslog_rotate,$syslog_size,$syslog_timeout,$syslog_local,$syslog_unique,$syslog_server) # Configure syslog parameters
    $esxcli.system.syslog.reload() # Reload syslog configuration
    $esxcli.network.firewall.ruleset.set($null,$true,"syslog") # Enable ESXi firewall rule for syslog service
    Write-Host "Modified Syslog settings for " $esx.name

    # ----- Dump Collector Settings -----
    $esxcli.system.coredump.network.set($null,$dump_interface,$dump_collector,$dump_port) # Configure Dump Collector
    $esxcli.system.coredump.network.set($true,$null,$null,$null) # Enable Dump Collector
    Write-Host "Modified Dump Collector settings for " $esx.name
}

#Disconnect vCenter Server
Disconnect-VIServer -Confirm:$false