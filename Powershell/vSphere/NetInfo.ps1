# vSphere Network Script
# Version 1.0
# Generates 1 CSV Files: 
# NetInfo contains the details for each vmnic (port id, VLANs, device)
# Created by Jorge Buitrago
# email: jorge.buitrago@virtesa.com

#Initialize report variables
$filename = "E:\PowerCLI\NetInfo.csv"
$vCServer = "vcenter.vlab.local"

#Connect to vCenter Server
Connect-VIServer $vCServer

Write "Gathering vmnic objects"

#Obtain list of connected and maintenance ESXi hosts
$vmhosts = Get-VMHost | Sort Name | Where-Object {$_.State -eq "Connected" -or $_.ConnectionState -eq "Maintenance"} | Get-View

#Initialize report variable
$MyCol = @()

#Loop through the ESXi hosts
foreach ($vmhost in $vmhosts){
 $ESXHost = $vmhost.Name
 Write "Collating information for $ESXHost"
 $networkSystem = Get-view $vmhost.ConfigManager.NetworkSystem
 #Loop through the vmnics
 foreach($pnic in $networkSystem.NetworkConfig.Pnic){
     $pnicInfo = $networkSystem.QueryNetworkHint($pnic.Device)
     #Loop through the vmnic View
     foreach($Hint in $pnicInfo){
         $NetworkInfo = "" | select-Object Host, PNic, Speed, MAC, DeviceID, PortID, VLAN
         $NetworkInfo.Host = $vmhost.Name
         $NetworkInfo.PNic = $Hint.Device
         $NetworkInfo.DeviceID = $Hint.connectedSwitchPort.DevId
         $NetworkInfo.PortID = $Hint.connectedSwitchPort.PortId
         $record = 0
         Do{
             If ($Hint.Device -eq $vmhost.Config.Network.Pnic[$record].Device){
                 $NetworkInfo.Speed = $vmhost.Config.Network.Pnic[$record].LinkSpeed.SpeedMb
                 $NetworkInfo.MAC = $vmhost.Config.Network.Pnic[$record].Mac
             }
             $record ++
         }
         #Loop through the vmnic View records
         Until ($record -eq ($vmhost.Config.Network.Pnic.Length))
         foreach ($obs in $Hint.Subnet){
            #Loop through the VLANs
             Foreach ($VLAN in $obs.VlanId){
                 If ($VLAN -eq $null){
                 }
                 Else{
                     $strVLAN = $VLAN.ToString()
                     $NetworkInfo.VLAN += $strVLAN + " "
                 }
             }
         }
         #Update fields on MyCol
         $MyCol += $NetworkInfo
     }
 }
}

#Export the report as CSV
$Mycol | Sort Host, PNic | Export-Csv $filename -NoTypeInformation

#Disconnect from vCenter Server
Disconnect-VIServer -Confirm:$false