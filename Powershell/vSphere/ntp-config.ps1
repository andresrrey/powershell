# vSphere NTP Script
# Version 1.0
# Configures an NTP server on all the ESXi hosts
# Created by Jorge Buitrago
# email: jorge.buitrago@virtesa.com

#Initialize Variables
$vcServer = "vcenter.vlab.local"
$ntpserver = "horalegal.sic.gov.co"

#Connect to vCenter
Connect-VIServer $vcServer 

#Get list of active hosts
$esxs = Get-VMHost | ?{$_.ConnectionState -eq "Connected" -or $_.ConnectionState -eq "Maintenance"} | sort

#Loop through the hosts
foreach ($esx in $esxs) {
	$ntp0=Get-VmHostNtpServer -VMHost $esx
    #Check if ntp server is already in the list
	if ($ntp0 -ne $ntpserver){
	   Add-VmHostNtpServer -NtpServer $ntpserver  -VmHost $esx
       Write "Added NTP Server to host $esx"
	}
    #Configure NTP service startup
    $ntp = Get-VmHostService -VMhost $esx | Where {$_.Key -eq 'ntpd'}
    Set-VMHostService -HostService $ntp -policy "automatic"
    Restart-Vmhostservice $ntp -Confirm:$false
    Write "Modified NTP service startup for host $esx"
}

#Disconnect from vCenter
Disconnect-VIServer $vcServer -Confirm:$false 