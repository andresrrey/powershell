# vSphere Windows 2003 scsi timeout Script
# Version 1.0
# Configures a new SCSI timeout value for all the Windows Server 2003 Virtual Machines
# Created by Jorge Buitrago
# email: jorge.buitrago@virtesa.com

#Initialize Variables
$vcServer = "vcenter.vlab.local"
$timeout = 90
#Connect to vCenter Server
Connect-VIServer $vcServer
#Get all Windows Server 2003 VMs
$VMs = Get-VM | ?{$_.Guest.OSFullName -like "*Windows Server 2003*"}
#Loop through the VMs
ForEach ($VM in $VMs) {
   $reg = [Microsoft.Win32.RegistryKey]::OpenRemoteBaseKey('LocalMachine', $VM.Guest.Hostname)
   Write-Host "Timeout Value (ms): "$VM.Guest.HostName "-" $reg.OpenSubKey("SYSTEM\CurrentControlSet\Services\Disk\").GetValue("TimeoutValue")
    #Set regkey in OS  
   $regKey= $reg.OpenSubKey("SYSTEM\CurrentControlSet\Services\Disk",$true)
   $regkey.SetValue('TimeoutValue',$timeout,'DWord')
 
   Write-Host "New Timeout Value:  "$VM.Guest.HostName "-" $reg.OpenSubKey("SYSTEM\CurrentControlSet\Services\Disk\").GetValue("TimeoutValue")
}
#Disconnect vCenter Server
Disconnect-VIServer -Confirm:$false