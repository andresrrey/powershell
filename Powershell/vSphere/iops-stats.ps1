# vSphere IOPS Script
# Version 1.0
# Generates 1 CSV Files: 
# IOPS_Report contains the IOPS statistics for all the VMs
# Created by Jorge Buitrago
# email: jorge.buitrago@virtesa.com

#Initialize report variables
$filename = "E:\PowerCLI\IOPS_Report.csv"
$vCServer = "vcenter.vlab.local"

#Connect to vCenter Server
Connect-VIServer $vCServer

#Set the timeframe for the report
$daysback = -2
#Initialize the stats variables
$stats = @()

$vms = Get-VM
#Loop through the VMs
foreach ($vm in $vms){
	$temp = "" | Select VM,WAVG,RAVG,WMAX,RMAX
	$temp.VM = $vm.Name
	$wval = (((Get-Stat $vm -stat "datastore.numberWriteAveraged.average" -Start (Get-Date).adddays($daysback) -Finish (Get-Date) ) | select -expandproperty Value)  | measure -average -max); 
  	$rval = (((Get-Stat $vm -stat "datastore.numberReadAveraged.average" -Start (Get-Date).adddays($daysback) -Finish (Get-Date) ) | select -expandproperty Value)  | measure -average -max);
	$temp.WAVG=$wval.average
	$temp.RAVG=$rval.average
	$temp.WMAX=$wval.maximum
	$temp.RMAX=$rval.maximum
    #Update fields on report
	$stats += $temp
}

#Export the report as CSV
$stats | Export-CSV $filename -NoTypeInformation

#Disconnect from vCenter Server
Disconnect-VIServer -Confirm:$false