# Veeam Reporter Script
# Version 1.0
# Generates 2 CSV Files: 
# Veeam_Report_Summary contains the main settings for each job (Name, # of incrementals, Schedule, Total Size)
# Veeam_Report_Detailed contains detailed information about each job (Name, Repository, Size of each backup size)
# Created by Jorge Buitrago
# email: jorge.buitrago@virtesa.com

# This script needs to be executed using the Veeam Powershell executable, found in the options menu
# Otherwise, uncomment the following line to add the Veeam PS Snapin:
# Add-PSSnapin -Name VeeamPSSnapIn -ErrorAction SilentlyContinue

#Initialize report variables
$Report = @()
$Report2 = @()

#If necessary, edit the location for the outputs:
$DetailedReportLocation = "C:\Temp\Veeam_Report_Detailed.csv"
$SummaryReportLocation = "C:\Temp\Veeam_Report_Summary.csv"

#Obtain backup jobs
$jobs = Get-VBRJob | ?{$_.JobType -eq "Backup"} |sort-Object Name

#Loop through all jobs
foreach ($job in $jobs){
	$jobid = $job.Info.Id
	$path = (Get-VBRBackup | ?{$_.Info.JobId -eq $jobid}).DirPath #get path for each job
	
	#Set Summary report fields
	$temp2 = ""|Select JobName,Repository,NumDeltas,AverageDeltas,MaxDeltas,MinDeltas,NumFulls,FullSize,TotalSpace,RetentionPoints,Frequency,Schedule,OldestBackup,LastSuccesfulRun,LastRunResult
	$temp2.JobName = $job.Name
	$temp2.Repository= $path
	$temp2.RetentionPoints = $job.BackupStorageOptions.RetainCycles
	
	#Find schedule (or full execution order for Sequential Jobs) and Frequency of the job, 
	if ($job.ScheduleOptions.OptionsScheduleAfterJob.IsEnabled){
		$temp2.Schedule = ""
		$thisJob=$job
		$scheduleFound = $false
		while (-Not $scheduleFound){
			$parent = Get-VBRJob | ?{$_.Id -eq $thisJob.Info.ParentScheduleID}
			if ($parent.ScheduleOptions.OptionsScheduleAfterJob.IsEnabled){
				$thisJob = $parent
				$temp2.Schedule = $parent.Name + "\" + $temp2.Schedule
			}
			else {
				$temp2.Schedule = (($parent.Info.ScheduleOptions.NextRun).Split(" "))[1] + "\" + $parent.Name + "\" + $temp2.Schedule
				if ($parent.Info.ScheduleOptions.OptionsDaily.Enabled){
					$temp2.Frequency = "Daily"
				}
				elseif ($parent.Info.ScheduleOptions.OptionsMonthly.Enabled){
					$temp2.Frequency = "Monthly"
				}
				elseif ($parent.Info.ScheduleOptions.OptionsPeriodically.Enabled){
					$temp2.Frequency = "Custom"
				}
				elseif ($parent.Info.ScheduleOptions.OptionsContinuous.Enabled){
					$temp2.Frequency = "Continuous"
				}
				else {
					$temp2.Frequency = "Starting Job not Scheduled"
				}
				$scheduleFound = $true
			}
		}
	}
	else {
		$temp2.Schedule = (($job.Info.ScheduleOptions.NextRun).Split(" "))[1]
		if ($job.Info.ScheduleOptions.OptionsDaily.Enabled){
			$temp2.Frequency = "Daily"
		}
		elseif ($job.Info.ScheduleOptions.OptionsMonthly.Enabled){
			$temp2.Frequency = "Monthly"
		}
		elseif ($job.Info.ScheduleOptions.OptionsPeriodically.Enabled){
			$temp2.Frequency = "Custom"
		}
		elseif ($job.Info.ScheduleOptions.OptionsContinuous.Enabled){
			$temp2.Frequency = "Continuous"
		}
		else {
			$temp2.Frequency = "Not Scheduled"
		}
	}
	
	#Get Last Succesful Run
	$sessionName = (Get-VBRBackup | ?{$_.Info.JobId -eq $jobid}).Name
	$sessions = Get-VBRBackupSession -Name ($sessionName + "*") | ?{$_.Result -eq "Success" -or $_.Result -eq "Warning"} | Sort-Object EndTime
	if ($sessions.length -eq $null){
		$temp2.LastSuccesfulRun = "Not found"
		$temp2.LastRunResult = "Not found"
	}
	else {
		$temp2.LastSuccesfulRun = $sessions[-1].EndTime
		$temp2.LastRunResult = $sessions[-1].Result
	}

	#Get vrb and vib files (Differential Backups) and obtain the stats for these files
	$deltas1 = dir $path *.vrb | Select Name,Length,LastWriteTime
	$deltas2 = dir $path *.vib | Select Name, Length,LastWriteTime
	$deltastemp = $deltas1 + $deltas2 | Sort-Object Length
	$sizes = @()
	foreach ($delta in $deltastemp){
		$sizes += $delta.Length
	}
	$measure = $sizes |Measure-Object -Average -Sum -Max -Min
	$temp2.NumDeltas = $measure.Count
	$temp2.AverageDeltas = "{0:N2}" -f ($measure.Average/1073741824)
	$temp2.MaxDeltas = "{0:N2}" -f ($measure.Maximum/1073741824)
	$temp2.MinDeltas = "{0:N2}" -f ($measure.Minimum/1073741824)

	#Get vbk files (Full backups) and obtain the stats for these files
	$deltas3 = dir $path *.vbk | Select Name, Length,LastWriteTime
	$sizes = @()
	foreach ($delta in $deltas3){
		$sizes += $delta.Length
	}
	$measure = $sizes |Measure-Object -Sum
	$temp2.NumFulls = $measure.Count
	$temp2.FullSize = "{0:N2}" -f ($measure.Sum/1073741824)
	
	$deltas = $deltas1 + $deltas2 + $deltas3 | Sort-Object Length
	$temp2.OldestBackup = ($deltas | Sort-Object LastWriteTime)[0].LastWriteTime
	$sizes = @()
	foreach ($delta in $deltas){
		$sizes += $delta.Length
	}
	$measure = $sizes |Measure-Object -Sum
	$temp2.TotalSpace = "{0:N2}" -f ($measure.Sum/1073741824)

	#Update fields on report2
	$Report2 += $temp2

	#Loop through all files belonging to each job 
	Foreach ($delta in $deltas){
		#Set Detailed Report fields
		$temp = "" | Select JobName,Path,FileName,Type,FileSizeGB
		$temp.JobName = $job.Name
		$temp.Path = $path
		$temp.FileName = ([io.fileinfo]$delta.Name).basename
		$ext = ([io.fileinfo]$delta.Name).extension
		if ($ext -eq ".vrb"){
			$temp.Type = "REVERSE"
		}
		elseif ($ext -eq ".vib") {
			$temp.Type = "INCREMENTAL"
		}
		else {
			$temp.Type = "FULL"
		}
		$sizeGB = ($delta.Length)/1073741824
		$temp.FileSizeGB = "{0:N2}" -f $sizeGB
		$Report += $temp
		}


}

#Export the reports as CSV files
$Report | Export-CSV $DetailedReportLocation -NoTypeInformation
$Report2 | Export-CSV $SummaryReportLocation -NoTypeInformation