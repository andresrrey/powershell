# VMware View Reporter Script
# Version 1.0
# Generates 1 CSV Files: 
# Connected_Users contains the connection details for all the active sessions in VMware View
# Created by Jorge Buitrago
# email: jorge.buitrago@virtesa.com

#This script must be executed from VMware View PowerCLI

#Get all desktop pools
$pools = Get-Pool

#Initialize Report variables
$ConnectedUsers = @()
$time= Get-Date
$filename = "E:\PowerCLI\ConnectedUsers.csv"

#Loop through the desktop pools
foreach ($pool in $pools){
    $users = Get-RemoteSession | ?{$_.pool_id -eq $pool.pool_id -and $_.state -eq "CONNECTED"}
    if ($users -ne $null){
        foreach ($user in $users){
            $temp = "" | Select Time,Pool,Username
            $temp.Time = $time
            $temp.Pool = $pool.pool_id
            $temp.Username = ($user.Username).split("\")[1]
            #Update fields on report
            $ConnectedUsers += $temp
        }
    }
}

#Export the report as CSV
$ConnectedUsers | Export-CSV $filename -NoTypeInformation